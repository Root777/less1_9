# Less1.9
## 1. Создать пользователя, назвать как нравится. Разрешить ему выполнять через sudo только одну программу (skukozh из вложения)
```bash
[root@CentOS_7 ~]# useradd alien
[root@CentOS_7 ~]# passwd alien
Changing password for user alien.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
[root@CentOS_7 ~]# ls -l /home/alien/
total 0
[root@CentOS_7 ~]# cp /home/skukozh /home/alien/
[root@CentOS_7 ~]# ls -l /home/alien/
total 12
-rw-r--r--. 1 root root 8520 Dec 20 13:28 skukozh
[root@CentOS_7 ~]# nano /etc/sudoers

%alien ALL = NOPASSWD: /home/alien/skukozh

[root@CentOS_7 ~]#
```
## 2. Выставить атрибуты на skukozh:
```bash
[root@CentOS_7 ~]# chmod 550 /home/alien/skukozh
[root@CentOS_7 ~]# ls -l /home/alien/
total 12
-r-xr-x---. 1 root root 8520 Dec 20 13:28 skukozh
```
## 3.1. Скопировать любой исполняемый файл (ls, bash, итд) в домашний каталог пользователя. 
```bash
[root@CentOS_7 ~]# su - alien 
[alien@CentOS_7 ~]$ ls -l
total 12
-r-xr-x---. 1 root  root    8520 Dec 20 13:28 skukozh
[alien@CentOS_7 ~]$ cp /usr/bin/ls /home/alien/
[alien@CentOS_7 ~]$ ls -l
total 128
-rwxr-xr-x. 1 alien alien 117608 Dec 20 13:32 ls
-r-xr-x---. 1 root  root    8520 Dec 20 13:28 skukozh
[alien@CentOS_7 ~]$ ./ls
ls  skukozh
```
## 3.2. Скукожить этот исполняемый файл, и убедиться, что он не работает ($ sudo skukozh ./bash , например).
```bash
[alien@CentOS_7 ~]$ sudo ./skukozh ./ls
[alien@CentOS_7 ~]$ ./ls
-bash: ./ls: cannot execute binary file
[alien@CentOS_7 ~]$
```
